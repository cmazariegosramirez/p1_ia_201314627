import os, copy

class Reversi:
    def __init__(self):
        self.boxes = 8
        self.board = [["2" for x in range(self.boxes)] for y in range(self.boxes)]
        self.dx = [-1, 0, 1, -1, 1, -1, 0, 1]
        self.dy = [-1, -1, -1, 0, 0, 1, 1, 1]
        self.player = None
        self.minEvalBoard = -1  
        self.maxEvalBoard = self.boxes * self.boxes + 4 * self.boxes + 4 + 1  
        self.depth = 4
    
    def getValorActual(self,num):
        if num == "2":
            return "0"
        if num == "1":
            return "2"
        if num == "0":
            return "1"


    def definirMatriz(self,boardString):
        myx = 0
        myy = 0
        pieces = list(boardString)
        for piece in pieces:
            self.board[myx][myy] = self.getValorActual(piece)
            if myx == 7:
                myx = -1
                myy = myy + 1
            myx = myx + 1


    def imprimirMatriz(self):
        boardstr = ""
        m = len(str(self.boxes - 1))
        for y in range(self.boxes):
            row = ""
            for x in range(self.boxes):
                boardstr += self.board[y][x]
                boardstr += " " * m
            boardstr += (row + " " + str(y) + "\n")
        print
        row = ""
        for x in range(self.boxes):
            row += str(x).zfill(m) + " " 
        boardstr += "\n" + row + "\n"
        return boardstr


    def mov1(self, board, x, y, player):
        totctr = 0
        board[y][x] = player
        for d in range(8):
            cx = 0
            for i in range(self.boxes):
                dx = x + self.dx[d] * (i + 1)
                dy = y + self.dy[d] * (i + 1)
                if dx < 0 or dx > self.boxes - 1 or dy < 0 or dy > self.boxes - 1:
                    cx = 0
                    break
                elif board[dy][dx] == player:
                    break
                elif board[dy][dx] == "0":
                    cx = 0
                    break
                else:
                    cx += 1
            for i in range(cx):
                dx = x + self.dx[d] * (i + 1)
                dy = y + self.dy[d] * (i + 1)
                board[dy][dx] = player
            totctr += cx
        return (board, totctr)


    def mov2(self,board, x, y, player):
        if x < 0 or x > self.boxes - 1 or y < 0 or y > self.boxes - 1:
            return False
        if board[y][x] != "0":
            return False
        (boardTemp, totctr) = self.mov1(copy.deepcopy(board), x, y, player)
        if totctr == 0:
            return False
        return True

    def evaluarMatriz(self,board, player):
        tot = 0
        for y in range(self.boxes):
            for x in range(self.boxes):
                if board[y][x] == player:
                    if (x == 0 or x == self.boxes - 1) and (y == 0 or y == self.boxes - 1):
                        tot += 4
                    elif (x == 0 or x == self.boxes - 1) or (y == 0 or y == self.boxes - 1):
                        tot += 2
                    else:
                        tot += 1
        return tot


    def esNodo(self,board, player):
        for y in range(self.boxes):
            for x in range(self.boxes):
                if self.mov2(board, x, y, player):
                    return False
        return True


    def getNodosOrdenados(self,board, player):
        sNodes = []
        for y in range(self.boxes):
            for x in range(self.boxes):
                if self.mov2(board, x, y, player):
                    (boardTemp, totctr) = self.mov1(copy.deepcopy(board), x, y, player)
                    sNodes.append((boardTemp, self.evaluarMatriz(boardTemp, player)))
        sNodes = sorted(sNodes, key=lambda node: node[1], reverse=True)
        sNodes = [node[0] for node in sNodes]
        return sNodes


    def minMax(self,board, player, depth, maximizingPlayer):
        if depth == 0 or self.esNodo(board, player):
            return self.evaluarMatriz(board, player)
        if maximizingPlayer:
            bestValue = self.minEvalBoard
            for y in range(self.boxes):
                for x in range(self.boxes):
                    if self.mov2(board, x, y, player):
                        (boardTemp, totctr) = self.mov1(copy.deepcopy(board), x, y, player)
                        v = self.minMax(boardTemp, player, depth - 1, False)
                        bestValue = max(bestValue, v)
        else:  # minimizingPlayer
            bestValue = self.maxEvalBoard
            for y in range(self.boxes):
                for x in range(self.boxes):
                    if self.mov2(board, x, y, player):
                        (boardTemp, totctr) = self.mov1(copy.deepcopy(board), x, y, player)
                        v = self.minMax(boardTemp, player, depth - 1, True)
                        bestValue = min(bestValue, v)
        return bestValue


    def mov4(self, board, player):
        maxPoints = 0
        mx = -1
        my = -1
        for y in range(self.boxes):
            for x in range(self.boxes):
                if self.mov2(board, x, y, player):
                    (boardTemp, totctr) = self.mov1(copy.deepcopy(board), x, y, player)
                    points = self.minMax(boardTemp, player, self.depth, True)
                    if points > maxPoints:
                        maxPoints = points
                        mx = x
                        my = y
        return (mx, my)

    def mov5(self,deep, play, board):
        self.depth = deep
        player = play
        self.definirMatriz(board)
        (x, y) = self.mov4(self.board, player)
        if not (x == -1 and y == -1):
            (self.board, totctr) = self.mov1(self.board, x, y, player)
            return str(x) + "" + str(y)