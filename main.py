from reversi.reversi import Reversi
from flask import Flask, request, render_template
from reversi.reversi import Reversi

app = Flask(__name__, template_folder='vistas')
reversi = Reversi()

@app.route("/")
def main():
    playerTurn = request.args.get('turno')
    matrixState = request.args.get('estado')
    return reversi.mov5(4,"1" if playerTurn == "0" else "2" ,matrixState+"")

@app.route("/viewboard")
def getboard():
    playerTurn = request.args.get('turno')
    matrixState = request.args.get('estado')
    reversi.mov5(4,"1" if playerTurn == "0" else "2" ,matrixState+"")
    return render_template("index.html",matriz=reversi.imprimirMatriz())

if __name__ == "__main__":
    app.run()